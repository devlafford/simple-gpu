	spi_master_jtag u0 (
		.clk50_clk                (<connected-to-clk50_clk>),                //               clk50.clk
		.rstn_reset_n             (<connected-to-rstn_reset_n>),             //                rstn.reset_n
		.spi_master_external_MISO (<connected-to-spi_master_external_MISO>), // spi_master_external.MISO
		.spi_master_external_MOSI (<connected-to-spi_master_external_MOSI>), //                    .MOSI
		.spi_master_external_SCLK (<connected-to-spi_master_external_SCLK>), //                    .SCLK
		.spi_master_external_SS_n (<connected-to-spi_master_external_SS_n>), //                    .SS_n
		.spi_master_irq_irq       (<connected-to-spi_master_irq_irq>)        //      spi_master_irq.irq
	);

