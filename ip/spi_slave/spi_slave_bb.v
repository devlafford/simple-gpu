
module spi_slave (
	spi_slave_clk_clk,
	spi_slave_external_mosi,
	spi_slave_external_nss,
	spi_slave_external_miso,
	spi_slave_external_sclk,
	spi_slave_rstn_reset_n,
	spi_slave_st_sink_valid,
	spi_slave_st_sink_data,
	spi_slave_st_sink_ready,
	spi_slave_st_source_ready,
	spi_slave_st_source_valid,
	spi_slave_st_source_data);	

	input		spi_slave_clk_clk;
	input		spi_slave_external_mosi;
	input		spi_slave_external_nss;
	inout		spi_slave_external_miso;
	input		spi_slave_external_sclk;
	input		spi_slave_rstn_reset_n;
	input		spi_slave_st_sink_valid;
	input	[7:0]	spi_slave_st_sink_data;
	output		spi_slave_st_sink_ready;
	input		spi_slave_st_source_ready;
	output		spi_slave_st_source_valid;
	output	[7:0]	spi_slave_st_source_data;
endmodule
