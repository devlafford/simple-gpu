	spi_slave2 u0 (
		.spi_slave_clk_clk        (<connected-to-spi_slave_clk_clk>),        //      spi_slave_clk.clk
		.spi_slave_rstn_reset_n   (<connected-to-spi_slave_rstn_reset_n>),   //     spi_slave_rstn.reset_n
		.spi_slave_ctl_writedata  (<connected-to-spi_slave_ctl_writedata>),  //      spi_slave_ctl.writedata
		.spi_slave_ctl_readdata   (<connected-to-spi_slave_ctl_readdata>),   //                   .readdata
		.spi_slave_ctl_address    (<connected-to-spi_slave_ctl_address>),    //                   .address
		.spi_slave_ctl_read_n     (<connected-to-spi_slave_ctl_read_n>),     //                   .read_n
		.spi_slave_ctl_chipselect (<connected-to-spi_slave_ctl_chipselect>), //                   .chipselect
		.spi_slave_ctl_write_n    (<connected-to-spi_slave_ctl_write_n>),    //                   .write_n
		.spi_slave_irq_irq        (<connected-to-spi_slave_irq_irq>),        //      spi_slave_irq.irq
		.spi_slave_external_MISO  (<connected-to-spi_slave_external_MISO>),  // spi_slave_external.MISO
		.spi_slave_external_MOSI  (<connected-to-spi_slave_external_MOSI>),  //                   .MOSI
		.spi_slave_external_SCLK  (<connected-to-spi_slave_external_SCLK>),  //                   .SCLK
		.spi_slave_external_SS_n  (<connected-to-spi_slave_external_SS_n>)   //                   .SS_n
	);

