`timescale 1ps/1ps

module tb_command_intf();

logic        clk;
logic        rst_n;
logic        mosi;
logic        miso;
logic        spi_clk;
logic        spi_select;
logic        framebuffer_enabled;
logic [31:0] framebuffer_location;
logic        cmd_error;
logic [31:0] status;
logic [7:0]  command;

logic [28:0] cmd_intf_address;
logic [63:0] cmd_intf_writedata;
logic        cmd_intf_write;



    command_intf cmd_intf(
        .clk(clk),
        .rst_n(rst_n),
        .mosi(mosi),
        .miso(miso),
        .spi_clk(spi_clk),
        .spi_select(spi_select),
        .framebuffer_enabled(framebuffer_enabled),
        .framebuffer_location(framebuffer_location),

        .cmd_intf_address(cmd_intf_address),
        .cmd_intf_burstcount(),
        .cmd_intf_waitrequest(1'b0),
        .cmd_intf_readdata(64'b0),
        .cmd_intf_readdatavalid(1'b0),
        .cmd_intf_read(),
        .cmd_intf_writedata(cmd_intf_writedata),
        .cmd_intf_byteenable(),
        .cmd_intf_write(cmd_intf_write),

        .cmd_error(cmd_error),
        .status(status),
        .command(command)
    );

always #1  clk <= ~clk;
always #1000 spi_clk <= ~spi_clk;

// commands must be flipped around using {<<{}} operator
reg [7:0] set_framebuffer[5];
reg [7:0] stream_memory[37];
reg [7:0] bits;
reg [7:0] num;

// memory model
reg [63:0] memory[4];
always_ff @(posedge clk) begin
    if (cmd_intf_write) memory[cmd_intf_address] <= cmd_intf_writedata;
end

initial begin

    // set frambuffer command
    set_framebuffer[0] = {<<{8'h81}};
    set_framebuffer[1] = {<<{8'hf0}};
    set_framebuffer[2] = {<<{8'h0d}};
    set_framebuffer[3] = {<<{8'hba}};
    set_framebuffer[4] = {<<{8'hbe}};

    // stream memory command
    stream_memory[0] = {<<{8'hC1}};
    stream_memory[1] = {<<{8'h00}};
    stream_memory[2] = {<<{8'h00}};
    stream_memory[3] = {<<{8'h05}};
    stream_memory[4] = {<<{8'h78}};
    for (int i = 5; i < $size(stream_memory); i=i+1) begin
        num = i - 5;
        bits = num[7:0];
        stream_memory[i] = {<<{bits}};
    end

    // initialize signals
    clk = 0;
    rst_n = 1;
    mosi = 0;
    spi_clk = 0;
    spi_select = 1;

    // reset sequence
    @(negedge clk);
    rst_n = 0;
    @(negedge clk);
    rst_n = 1;
    @(negedge clk);

    // spi active
    @(negedge spi_clk);
    spi_select = 0;
    // send bits MSB first
    for (int i = 0; i < $size(set_framebuffer); i=i+1) begin
        for (int j = 0; j < 8; j = j+1) begin
            mosi = set_framebuffer[i][j];
            @(negedge spi_clk);
        end
    end
    for (int i = 0; i < $size(stream_memory); i=i+1) begin
        for (int j = 0; j < 8; j = j+1) begin
            mosi = stream_memory[i][j];
            @(negedge spi_clk);
        end
    end

    // let it sit idle for a bit
    spi_select = 1;
    repeat (200) @(negedge spi_clk);

    // show framebuffer location
    $display("framebuffer location: %x", framebuffer_location );
    for (int i = 0; i < $size(memory); i=i+1) begin
        $display("memory[%d] : %x", i, memory[i]);
    end
    $stop;

    

end


endmodule
