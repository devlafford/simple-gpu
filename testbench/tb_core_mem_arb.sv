localparam NUM_CHANNELS = 4;
module tb_core_mem_arb();

logic clk;
logic rst_n;

// from channel ifetch
logic [31:0] program_counter[NUM_CHANNELS];
logic [31:0] instruction[NUM_CHANNELS];
logic        ifetch[NUM_CHANNELS];
logic        fetched[NUM_CHANNELS];

// TODO: loadstore
logic        load[NUM_CHANNELS];
logic        store[NUM_CHANNELS];
logic [31:0] loadstore_address[NUM_CHANNELS];
logic [31:0] loadstore_readdata[NUM_CHANNELS];
logic [31:0] loadstore_writedata[NUM_CHANNELS];
logic        loadstore_served[NUM_CHANNELS];

// ddr3 interface
logic [28:0] core_address;
logic [7:0]  core_burstcount;
logic        core_waitrequest;
logic [63:0] core_readdata;
logic        core_readdatavalid;
logic        core_read;
logic [63:0] core_writedata;
logic [7:0]  core_byteenable;
logic        core_write;

core_mem_arb #(
    .NUM_CHANNELS(NUM_CHANNELS)
) dut (

    .clk                    (clk),
    .rst_n                  (rst_n),
    .program_counter        (program_counter),
    .instruction            (instruction),
    .ifetch                 (ifetch),
    .fetched                (fetched),

    .load                   (load),
    .store                  (store),
    .loadstore_address      (loadstore_address),
    .loadstore_readdata     (loadstore_readdata),
    .loadstore_writedata    (loadstore_writedata),
    .loadstore_served       (loadstore_served),

    .core_address           (core_address),
    .core_burstcount        (core_burstcount),
    .core_waitrequest       (core_waitrequest),
    .core_readdata          (core_readdata),
    .core_readdatavalid     (core_readdatavalid),
    .core_read              (core_read),
    .core_writedata         (core_writedata),
    .core_byteenable        (core_byteenable),
    .core_write             (core_write)
);

always #10 clk <= ~clk;

initial begin
    clk = 0;
    rst_n = 1;
    program_counter = {32'h4000, 32'h3000, 32'h2000, 32'h1000};
    ifetch = {0,0,0,0};
    load = {0,0,0,0};
    store = {0,0,0,0};
    loadstore_address = {32'h4004, 32'h3004, 32'h2004, 32'h1004};
    loadstore_writedata = {0,0,0,0};
    core_waitrequest = 1;
    core_readdata = 0;
    core_readdatavalid = 0;
    
    // reset sequence
    @(negedge clk);
    rst_n = 0;
    @(negedge clk);
    rst_n = 1;
    @(negedge clk);

    // wait a while and make sure nothing happens
    repeat (10) @(negedge clk);

    // pretend that a warp in channel 2 has become active, was scheduled, and is fetching an instruction
    ifetch[2] = 1;
    @(negedge clk);

    // make sure we wait until ddr3 is ready and check that request is made
    repeat (10) @(negedge clk);
    core_waitrequest = 1;
    #1 // combinational delay
    if (core_read !== 0) $display("Test failed at line %d; core_read = %x", `__LINE__, core_read);

    // core is eventually ready
    repeat (10) @(negedge clk);
    core_waitrequest = 0;
    #1 // combinational delay
    if (core_read !== 1) $display("Test failed at line %d; core_read = %x", `__LINE__, core_read);

    // load comes back eventually
    repeat (10) @(negedge clk);
    core_readdatavalid = 1;
    core_readdata = 64'hdead_beef_feed_dead; // made up instruction
    #1 // combinational delay
    if (instruction[2] !== 32'hfeed_dead) $display("Test failed at line %d; instruction = %x", `__LINE__, instruction[2]);
    @(negedge clk);
    core_readdatavalid = 0;
    core_readdata = 64'h0000_0000_0000_0000;

    // fetch next instruction for channel 0, still no other warps
    program_counter[2] = program_counter[0] + 4;
    @(negedge clk);
    if (core_read !== 1) $display("Test failed at line %d; core_read = %x", `__LINE__, core_read);

    // load comes back eventually
    repeat (1) @(negedge clk);
    core_readdatavalid = 1;
    core_readdata = 64'hdead_beef_feed_dead; // made up instruction
    #1 // combinational delay
    if (instruction[2] !== 32'hdead_beef) $display("Test failed at line %d; instruction = %x", `__LINE__, instruction[2]);
    @(negedge clk);
    core_readdatavalid = 0;
    core_readdata = 64'h0000_0000_0000_0000;

    // try a load from channel 2
    ifetch = {0,0,0,0};
    load[2] = 1;
    @(negedge clk);
    if (core_read !== 1) $display("Test failed at line %d; core_read = %x", `__LINE__, core_read);
    @(negedge clk);
    // load comes back eventually
    core_readdatavalid = 1;
    core_readdata = 64'hdead_beef_feed_dead; // made up instruction
    #1 // combinational delay
    if (loadstore_readdata[2] !== 32'hdead_beef) $display("Test failed at line %d; instruction = %x", `__LINE__, instruction[2]);
    @(negedge clk);
    core_readdatavalid = 0;
    core_readdata = 64'h0000_0000_0000_0000;


    // idle a little while
    repeat (10) @(negedge clk);

    $display("Test completed.");
    $stop;
    
    


end


endmodule
