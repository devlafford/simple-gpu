module tb_warp_channel();

localparam MAX_WARPS = 64;

logic clk;
logic rst_n;
logic set_active;
logic [$clog2(MAX_WARPS)-1:0] new_active_warp;
logic active;
logic [$clog2(MAX_WARPS)-1:0] active_warp;

logic [31:0] mask_write;
logic mask_read;
logic [31:0] mask_dstreg_data;
logic [31:0] mask_src0_data;
logic [31:0] mask_src1_data;

logic [31:0] lanes_enabled;
logic        retire_channel;

logic [31:0] program_counter;
logic        set_program_counter;
logic [31:0] next_program_counter;
logic [31:0] instruction;
logic        ifetch;
logic        fetched;
 
logic [31:0] dstreg_write;
logic src0_read;
logic src1_read;
logic [4:0]  dstreg_addr;
logic [4:0]  src0_addr;
logic [4:0]  src1_addr;
logic [1023:0] src0_data;
logic [1023:0] src1_data;
logic [1023:0] dstreg_data;

logic        load;
logic        store;
logic [31:0] loadstore_address;
logic [31:0] loadstore_readdata;
logic [31:0] loadstore_writedata;
logic        loadstore_served;

warp_channel #(
    .MAX_WARPS(MAX_WARPS)
) u_warp_channel (
    .clk             (clk),
    .rst_n           (rst_n),
    .set_active      (set_active),
    .new_active_warp (new_active_warp),
    .active          (active),
    .active_warp     (active_warp),
    .retire_channel  (retire_channel),

    .mask_write       (mask_write),
    .mask_read        (mask_read),
    .mask_dstreg_data (mask_dstreg_data),
    .mask_src0_data   (mask_src0_data),
    .mask_src1_data   (mask_src1_data),

    .lanes_enabled (lanes_enabled),

    .program_counter      (program_counter),  // to icache
    .set_program_counter  (set_program_counter),
    .next_program_counter (next_program_counter),
    .instruction          (instruction), // from icache
    .ifetch               (ifetch),
    .fetched              (fetched),

    .dstreg_write    (dstreg_write),
    .src0_read       (src0_read),
    .src1_read       (src1_read),
    .dstreg_addr     (dstreg_addr),
    .src0_addr       (src0_addr),
    .src1_addr       (src1_addr),
    .src0_data       (src0_data),
    .src1_data       (src1_data),
    .dstreg_data     (dstreg_data),

    .load                (load),
    .store               (store),
    .loadstore_address   (loadstore_address),
    .loadstore_readdata  (loadstore_readdata),
    .loadstore_writedata (loadstore_writedata),
    .loadstore_served    (loadstore_served)

);

always #1 clk <= ~clk;
initial begin

    clk = 0;
    rst_n = 1;
    set_active = 0;
    new_active_warp = 0;
    mask_src0_data = 0;
    mask_src1_data = 1;
    lanes_enabled = 32'h0000ffff; // only lanes 15:0 enabled
    instruction = 0;

    // simulate register file being full of its position in the warp
    for (int i = 0 ; i < 32; i = i + 1) begin
        src0_data[32*i+:32] = i;
        src1_data[32*i+:32] = i;
    end

    // reset sequence
    @(negedge clk);
    rst_n = 0;
    @(negedge clk);
    rst_n = 1;

    // set the channel to active, pretend we gave it warp 4
    set_active = 1;
    new_active_warp = 4;
    @(negedge clk);
    set_active = 0;
    new_active_warp = 0;
    if (active !== 1) $display("Test failed at line %d; active = %x", `__LINE__, active);
    if (active_warp !== 4) $display("Test failed at line %d; active_warp = %x", `__LINE__, active_warp);

    // Try unconditional ADD r1, r2, r3 instruction
    instruction = 32'b1_00000000_00001_00010_00011_00000000;
    @(negedge clk);
    if (dstreg_write !== 32'hffffffff) $display("Test failed at line %d; dstreg_write = %x", `__LINE__, dstreg_write);
    if (dstreg_addr  !== 5'd1) $display("Test failed at line %d; dstreg_write = %x", `__LINE__, dstreg_write);
    if (src0_addr    !== 5'd2) $display("Test failed at line %d; src0_addr = %x", `__LINE__, src0_addr);
    if (src1_addr    !== 5'd3) $display("Test failed at line %d; src1_addr = %x", `__LINE__, src1_addr);
    for (int i = 0 ; i < 32; i = i + 1) begin
        if (dstreg_data[32*i+:32] !== 2*i) $display("Test failed at line %d; dstreg_data[%d]= %x", `__LINE__, i, dstreg_data[i]);
    end
    @(negedge clk);

    // Try conditional ADD r1, r2, r3 instruction
    instruction = 32'b0_00000000_00001_00010_00011_00000000;
    @(negedge clk);
    if (dstreg_write !== 32'h0000ffff) $display("Test failed at line %d; dstreg_write = %x", `__LINE__, dstreg_write);
    if (dstreg_addr  !== 5'd1) $display("Test failed at line %d; dstreg_write = %x", `__LINE__, dstreg_write);
    if (src0_addr    !== 5'd2) $display("Test failed at line %d; src0_addr = %x", `__LINE__, src0_addr);
    if (src1_addr    !== 5'd3) $display("Test failed at line %d; src1_addr = %x", `__LINE__, src1_addr);
    for (int i = 0 ; i < 32; i = i + 1) begin
        if (dstreg_data[32*i+:32] !== 2*i) $display("Test failed at line %d; dstreg_data[%d]= %x", `__LINE__, i, dstreg_data[i]);
    end

    // Try unconditional EQ m1, r1, r1 instruction
    instruction = 32'b1_00010000_00001_00001_00001_00000000;
    @(negedge clk);
    if (dstreg_write !== 32'h00000000) $display("Test failed at line %d; dstreg_write = %x", `__LINE__, dstreg_write);
    if (mask_write   !== 32'hffffffff) $display("Test failed at line %d; mask_write = %x", `__LINE__, mask_write);
    if (dstreg_addr  !== 5'd1) $display("Test failed at line %d; dstreg_write = %x", `__LINE__, dstreg_write);
    if (src0_addr    !== 5'd1) $display("Test failed at line %d; src0_addr = %x", `__LINE__, src0_addr);
    if (src1_addr    !== 5'd1) $display("Test failed at line %d; src1_addr = %x", `__LINE__, src1_addr);
    for (int i = 0 ; i < 32; i = i + 1) begin
        if (mask_dstreg_data[i] !== 1) $display("Test failed at line %d; mask_dstreg_data[%d]= %x", `__LINE__, i, mask_dstreg_data[i]);
    end

    // Try conditional EQ m1, r1, r1 instruction
    instruction = 32'b0_00010000_00001_00001_00001_00000000;
    @(negedge clk);
    if (dstreg_write !== 32'h00000000) $display("Test failed at line %d; dstreg_write = %x", `__LINE__, dstreg_write);
    if (mask_write   !== 32'h0000ffff) $display("Test failed at line %d; mask_write = %x", `__LINE__, mask_write);
    if (dstreg_addr  !== 5'd1) $display("Test failed at line %d; dstreg_write = %x", `__LINE__, dstreg_write);
    if (src0_addr    !== 5'd1) $display("Test failed at line %d; src0_addr = %x", `__LINE__, src0_addr);
    if (src1_addr    !== 5'd1) $display("Test failed at line %d; src1_addr = %x", `__LINE__, src1_addr);
    for (int i = 0 ; i < 32; i = i + 1) begin
        if (mask_dstreg_data[i] !== 1) $display("Test failed at line %d; mask_dstreg_data[%d]= %x", `__LINE__, i, mask_dstreg_data[i]);
    end

    // TODO: more warp_channel isntruction tests
    // verify instruction fetch behavior
    // try a mask operation
    // try a loadstore operation



    $display("Test completed.");
    $stop;

end

endmodule
