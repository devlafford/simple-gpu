
localparam NUM_CHANNELS = 4;
localparam MAX_WARPS = 8;

module tb_warp_scheduler();

logic        clk;
logic        rst_n;
logic        start_kernel;
logic        set_threadcount;
logic [18:0] new_threadcount;
logic        kernel_active;

logic [$clog2(MAX_WARPS)-1:0]    channel_selector[NUM_CHANNELS];
logic [$clog2(NUM_CHANNELS)-1:0] context_selector[MAX_WARPS];

   // warp contexts
logic [MAX_WARPS-1:0] set_context_enabled;
logic [MAX_WARPS-1:0] clr_context_enabled;
logic [MAX_WARPS-1:0] context_enabled;

logic [MAX_WARPS-1:0] set_context_active;
logic [MAX_WARPS-1:0] clr_context_active;
logic [MAX_WARPS-1:0] context_active;

// channels
logic [NUM_CHANNELS-1:0] set_channel_active;

// from writeback probably
logic [NUM_CHANNELS-1:0] retire_channels;


warp_scheduler #(
    .MAX_WARPS(MAX_WARPS),
    .NUM_CHANNELS(NUM_CHANNELS)
) dut (
    .clk                  (clk),
    .rst_n                (rst_n),
    .start_kernel         (start_kernel),
    .set_threadcount      (set_threadcount),
    .new_threadcount      (new_threadcount),
    .kernel_active        (kernel_active),
    
    .channel_selector     (channel_selector),
    .context_selector     (context_selector),

    .set_context_enabled     (set_context_enabled),
    .clr_context_enabled     (clr_context_enabled),
    .set_context_active      (set_context_active),
    .clr_context_active      (clr_context_active),
    .context_active          (context_active),

    .set_channel_active   (set_channel_active),

    .retire_channels(retire_channels)
);

// simulate enabled/active regs of contexts
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) context_enabled <= 0;
    else context_enabled <= (context_enabled | set_context_enabled) &~ clr_context_enabled;
    if (~rst_n) context_active <= 0;
    else context_active <= (context_active | set_context_active) &~ clr_context_active;
end

always #1 clk <= ~clk;

initial begin

    clk = 0;
    rst_n = 1;
    start_kernel = 0;
    set_threadcount = 0;
    new_threadcount = 0;
    retire_channels = 0;

    // reset
    @(negedge clk);
    rst_n = 0;
    @(negedge clk);
    rst_n = 1;

    // idle for a little while
    repeat (10) @(negedge clk);

    // set threadcount
    set_threadcount = 1;
    new_threadcount = 256;
    @(negedge clk);
    set_threadcount = 0;
    new_threadcount = 0;

    // idle for a little while
    repeat (10) @(negedge clk);

    // start a kernel
    start_kernel = 1;
    @(negedge clk);
    start_kernel = 0;
    @(negedge clk);

    // run for a little while
    repeat (1000) @(negedge clk);

    // cycle through issuing and retiring warps
    repeat (10) begin
        // retire a few warps
        retire_channels = 4'b0001;
        @(negedge clk);
        retire_channels = 4'b0010;
        @(negedge clk);
        retire_channels = 4'b0100;
        @(negedge clk);
        retire_channels = 4'b1000;
        @(negedge clk);
        retire_channels = 4'b0000;

        // run for a little while
        repeat (1000) @(negedge clk);
    end

    // gonna have to look at the signals directly for this one to make sure timings are correct

    $stop;
    

end

endmodule
