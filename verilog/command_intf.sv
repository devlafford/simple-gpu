

module command_intf (

    input  clk,
    input  rst_n,

    input  mosi,
    output miso,
    input  spi_clk,
    input  spi_select,

    // memory interface
    output [28:0] cmd_intf_address,
    output [7:0]  cmd_intf_burstcount,
    input         cmd_intf_waitrequest,
    input  [63:0] cmd_intf_readdata,
    input         cmd_intf_readdatavalid,
    output        cmd_intf_read,
    output [63:0] cmd_intf_writedata,
    output [7:0]  cmd_intf_byteenable,
    output        cmd_intf_write,

    // configurable registers
    output reg framebuffer_enabled,
    output reg [31:0] framebuffer_location,

    // errors
    output reg cmd_error,

    // debug
    output [31:0] status,
    output [7:0]  command 
);

wire [7:0] cmd_buffer_din;
reg        cmd_buffer_insert;
reg        cmd_buffer_remove;
wire [7:0] cmd_buffer_dout;
wire       cmd_buffer_empty;
wire       cmd_buffer_full;

wire resp_valid;
wire resp_data;
wire resp_ready;

buffer8 #(
    .BUFFER_DEPTH(256)
) cmd_buffer (
    .clk(clk),
    .rst_n(rst_n),
    .din(cmd_buffer_din),
    .insert(cmd_buffer_insert),
    .remove(cmd_buffer_remove),
    .dout(cmd_buffer_dout),
    .empty(cmd_buffer_empty),
    .full(cmd_buffer_full)
);

spi_slv spislv (
    /* input           */ .spi_slave_clk_clk(clk),
    /* input           */ .spi_slave_rstn_reset_n(rst_n),
    /* input           */ .spi_slave_external_mosi(mosi),
    /* input           */ .spi_slave_external_nss(spi_select),
    /* inout           */ .spi_slave_external_miso(miso),
    /* input           */ .spi_slave_external_sclk(spi_clk),
    /* input           */ .spi_slave_st_source_ready(~cmd_buffer_full),
    /* output          */ .spi_slave_st_source_valid(cmd_buffer_insert),
    /* output  [7:0]   */ .spi_slave_st_source_data(cmd_buffer_din),
    /* input           */ .spi_slave_st_sink_valid(1'b0),
    /* input   [7:0]   */ .spi_slave_st_sink_data(8'h0),
    /* output          */ .spi_slave_st_sink_ready()
);

// --- COMMAND/DATA HOLDING FLOPS --- //
reg  [7:0] cmd;
reg  [7:0] d0;
reg  [7:0] d1;
reg  [7:0] d2;
reg  [7:0] d3;
reg store_cmd;
reg store_d0;
reg store_d1;
reg store_d2;
reg store_d3;
reg digested;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) begin
        cmd <= 0;
        d0 <= 0;
        d1 <= 0;
        d2 <= 0;
        d3 <= 0;
    end
    else if (digested) begin
        cmd <= 0;
        d0 <= 0;
        d1 <= 0;
        d2 <= 0;
        d3 <= 0;
    end
    else begin
        if (store_cmd) cmd <= cmd_buffer_dout;
        if (store_d0)  d0  <= cmd_buffer_dout;
        if (store_d1)  d1  <= cmd_buffer_dout;
        if (store_d2)  d2  <= cmd_buffer_dout;
        if (store_d3)  d3  <= cmd_buffer_dout;
    end
end

reg [7:0] last_byte;
always_ff @(posedge clk, negedge rst_n) begin
    if(~rst_n) last_byte <= 0;
    else if (cmd_buffer_remove) last_byte <= cmd_buffer_dout;
    else last_byte <= last_byte;
end
assign command = last_byte;

// --- ERRORS --- //
reg  set_cmd_error;
reg  clr_cmd_error;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) cmd_error <= 0;
    else if (set_cmd_error) cmd_error <= 1;
    else if (clr_cmd_error) cmd_error <= 0;
    else cmd_error <= cmd_error;
end

// --- CONFIGURABLE REGISTERS --- //
reg [31:0] new_framebuffer_location;
reg        new_framebuffer_enabled;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) framebuffer_location <= 0;
    else framebuffer_location <= new_framebuffer_location;

    if (~rst_n) framebuffer_enabled <= 1;
    else framebuffer_enabled <= new_framebuffer_enabled;
end

// --- COMMANDS WITH STATE --- //

// memory pointer
reg [31:0] memory_pointer;
wire       inc_memory_pointer;
reg        set_memory_pointer;
reg [31:0] new_memory_pointer;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) memory_pointer <= 0;
    else if (inc_memory_pointer) memory_pointer <= memory_pointer + 1;
    else if (set_memory_pointer) memory_pointer <= new_memory_pointer;
    else memory_pointer <= memory_pointer;
end

// nbyte commands
reg streaming_to_memory;
reg set_streaming_to_memory;
reg clr_streaming_to_memory;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) streaming_to_memory <= 0;
    else if (set_streaming_to_memory) streaming_to_memory <= 1;
    else if (clr_streaming_to_memory) streaming_to_memory <= 0;
    else streaming_to_memory <= streaming_to_memory;
end

// --- NBYTE ENGINE (or is this memory? probably only ever used for memory, so synonym?) --- //
reg [7:0] nbyte_buf[8];
reg [3:0] nbyte_buf_index;
wire      nbyte_buf_full;
reg       nbyte_buf_insert;
reg       nbyte_buf_purge;
wire      write;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) for (integer i = 0; i < 8; i = i + 1) nbyte_buf[i] <= 0;
    else if (nbyte_buf_insert) nbyte_buf[nbyte_buf_index] <= cmd_buffer_dout;

    // counts down so bytes are in the right order
    if (~rst_n) nbyte_buf_index <= 0;
    else if (nbyte_buf_purge) nbyte_buf_index <= 0;
    else if (write) nbyte_buf_index <= 0; // write out
    else if (nbyte_buf_insert) nbyte_buf_index <= nbyte_buf_index + 1;
    else nbyte_buf_index <= nbyte_buf_index;
end
assign nbyte_buf_full = nbyte_buf_index == 8;
assign write = nbyte_buf_full && ~cmd_intf_waitrequest;
assign inc_memory_pointer = write;

assign cmd_intf_burstcount = 1;
assign cmd_intf_byteenable = 8'b11111111;

assign cmd_intf_address = memory_pointer;
assign cmd_intf_write = write;
assign cmd_intf_writedata = {nbyte_buf[0], nbyte_buf[1], nbyte_buf[2], nbyte_buf[3], nbyte_buf[4], nbyte_buf[5], nbyte_buf[6], nbyte_buf[7]};

assign cmd_intf_read = 0; // TODO: UNIMPLEMENTED

// --- STATE MACHINE --- //
typedef enum reg [6:0] {
    IDLE,
    TWO_D0,                                             // byte is the command data
    FIVE_D0,  FIVE_D1,  FIVE_D2,  FIVE_D3,              // four bytes are the command data
    NBYTE_D0, NBYTE_D1, NBYTE_D2, NBYTE_D3, NBYTE_DN,   // four bytes specify how large the command data is; N is the data
    ARBITRARY,
    PROCESSCMD
} cmd_state_t; // TODO: make sure the bit count is correct
cmd_state_t state, next_state;

always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) state <= IDLE;
    else state <= next_state;
end

// data counter
reg [31:0] data_counter;
reg        dec_data_counter;
reg        set_data_counter;
reg [31:0] new_data_counter;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) data_counter <= 0;
    else if (dec_data_counter) data_counter <= data_counter - 1;
    else if (set_data_counter) data_counter <= new_data_counter;
    else data_counter <= data_counter;
end
assign status = memory_pointer;

always_comb begin
    next_state = IDLE;

    cmd_buffer_remove = 0;

    store_cmd = 0;
    store_d0  = 0;
    store_d1  = 0;
    store_d2  = 0;
    store_d3  = 0;
    digested  = 0;

    // maintain error regs
    set_cmd_error = 0;
    clr_cmd_error = 0;

    // commands with state
    set_memory_pointer = 0;
    new_memory_pointer = memory_pointer;

    set_streaming_to_memory = 0;
    clr_streaming_to_memory = 0;
    nbyte_buf_insert = 0;
    nbyte_buf_purge = 0;

    // maintain configuration registers
    dec_data_counter = 0;
    set_data_counter = 0;
    new_data_counter = data_counter;

    new_framebuffer_enabled = framebuffer_enabled;
    new_framebuffer_location = framebuffer_location;

    case(state)
// ----------------------------------------------------------------------------
    IDLE: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_cmd = 1;

            case(cmd_buffer_dout[7:6])
                2'b00: begin
                    if (cmd_buffer_dout == 0) next_state = IDLE; // ignore zero command
                    else next_state = ARBITRARY;
                end
                2'b01: next_state = TWO_D0;
                2'b10: next_state = FIVE_D0;
                2'b11: next_state = NBYTE_D0;
            endcase
        end
        else next_state = IDLE;
    end
// ----------------------------------------------------------------------------
    TWO_D0: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d0 = 1;
            next_state = PROCESSCMD;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    FIVE_D0: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d0 = 1;
            next_state = FIVE_D1;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    FIVE_D1: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d1 = 1;
            next_state = FIVE_D2;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    FIVE_D2: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d2 = 1;
            next_state = FIVE_D3;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    FIVE_D3: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d3 = 1;
            next_state = PROCESSCMD;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    NBYTE_D0: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d0 = 1;
            next_state = NBYTE_D1;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    NBYTE_D1: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d1 = 1;
            next_state = NBYTE_D2;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    NBYTE_D2: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d2 = 1;
            next_state = NBYTE_D3;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    NBYTE_D3: begin
        if (~cmd_buffer_empty) begin
            cmd_buffer_remove = 1;
            store_d3 = 1;
            next_state = PROCESSCMD;
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    NBYTE_DN: begin
        if (data_counter == 0 && ~cmd_intf_waitrequest) begin // dump buffer
            // cleanup & leave; clear all nbyte command state
            clr_streaming_to_memory = 1;
            nbyte_buf_purge = 1;
            next_state = IDLE; // leave
        end
        else if (data_counter != 0 && ~cmd_buffer_empty && ~nbyte_buf_full) begin // fill buffer
            cmd_buffer_remove = 1;
            dec_data_counter = 1;
            nbyte_buf_insert = 1;
            next_state = state; // stay
        end
        else begin
            next_state = state; // stay
        end
    end
// ----------------------------------------------------------------------------
    ARBITRARY: begin // TODO: there will probably be more to this
        next_state = PROCESSCMD;
    end
// ----------------------------------------------------------------------------
    PROCESSCMD: begin // decode the command, do its action
        next_state = IDLE;
        // digested = 1;
        case(cmd)
            // Arbitrary
            8'b00000000: ; // do nothing
            8'b00000001: clr_cmd_error = 1;
            8'b00000010: set_cmd_error = 1;
            8'b00000011: nbyte_buf_purge = 1;

            // 1 Byte data
            8'b01000000: ; // do nothing

            // 4 Byte data
            8'b10000000: ; // do nothing
            8'b10000001: new_framebuffer_location = {d0, d1, d2, d3};
            8'b10000010: begin // set write pointer
                set_memory_pointer = 1;
                new_memory_pointer = {d0, d1, d2, d3}; // TODO: this probably needs to be shifted bc avalon
            end
            8'b10000010: begin // set thread count
                // TODO
            end

            // N Byte data
            8'b11000000: ; // do nothing
            8'b11000001: begin // stream to memory
                set_streaming_to_memory = 1;
                set_data_counter = 1;
                new_data_counter = {d0, d1, d2, d3};
                next_state = NBYTE_DN;
            end

            // Not a real command
            default: set_cmd_error = 1;
        endcase
    end
    default: begin
        next_state = IDLE;
        digested = 1;
    end
    endcase
end








endmodule
