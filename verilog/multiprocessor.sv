module multiprocessor #(
    parameter MAX_WARPS = 8,
    parameter NUM_CHANNELS = 2,
    parameter MASKS_PER_THREAD = 4
) (
    input clk,
    input rst_n,

    // command interface
    input  start_kernel,
    input  [31:0] kernel_address,
    output kernel_active, // block the command interface
    input  set_threadcount,
    input  [$clog2(MAX_WARPS*32):0] new_threadcount,

    // ddr3_interface
    output [28:0] core_address,
    output [7:0]  core_burstcount,
    input         core_waitrequest,
    input  [63:0] core_readdata,
    input         core_readdatavalid,
    output        core_read,
    output [63:0] core_writedata,
    output [7:0]  core_byteenable,
    output        core_write,

    input extra_input_that_does_nothing_but_fix_a_dumb_compile_error_I_always_forget
    
);


// --- WARP CONTEXTS --- //
wire [MAX_WARPS-1:0] ctxt_set_enabled;
wire [MAX_WARPS-1:0] ctxt_clr_enabled;
wire [MAX_WARPS-1:0] ctxt_enabled;
wire [MAX_WARPS-1:0] ctxt_set_active;
wire [MAX_WARPS-1:0] ctxt_clr_active;
wire [MAX_WARPS-1:0] ctxt_active;
wire                 ctxt_set_program_counter[MAX_WARPS];
wire [31:0]          ctxt_next_program_counter[MAX_WARPS];
wire [31:0]          ctxt_program_counter[MAX_WARPS];

wire [31:0]          ctxt_mask_write[MAX_WARPS];
wire                 ctxt_mask_read[MAX_WARPS];
wire [31:0]          ctxt_mask_dstreg_data[MAX_WARPS];
wire [31:0]          ctxt_mask_rd0_data[MAX_WARPS];
wire [31:0]          ctxt_mask_rd1_data[MAX_WARPS];

wire [31:0]          ctxt_lanes_enabled[MAX_WARPS];

// contexts and register file share these
wire [4:0] ctxt_dstreg_addr[MAX_WARPS];
wire [4:0] ctxt_src0_addr[MAX_WARPS];
wire [4:0] ctxt_src1_addr[MAX_WARPS];

genvar i;
generate
for (i = 0; i < MAX_WARPS; i = i + 1) begin : MULTIPROCESSOR_WARP_CONTEXTS
    warp_context u_warp_context(
        .clk                 (clk),
        .rst_n               (rst_n),
        .set_enabled         (ctxt_set_enabled[i]),
        .clr_enabled         (ctxt_clr_enabled[i]),
        .enabled             (ctxt_enabled[i]),
        .set_active          (ctxt_set_active[i]),
        .clr_active          (ctxt_clr_active[i]),
        .active              (ctxt_active[i]),
        .set_program_counter (start_kernel | ctxt_set_program_counter[i]),
        .new_program_counter (start_kernel ? kernel_address : ctxt_next_program_counter[i]),
        .program_counter     (ctxt_program_counter[i]),

        .mask_write          (ctxt_mask_write[i]),
        .mask_read           (ctxt_mask_read[i]),
        .mask_wr_addr        (ctxt_dstreg_addr[i]),
        .mask_rd0_addr       (ctxt_src0_addr[i]),
        .mask_rd1_addr       (ctxt_src0_addr[i]),
        .mask_wr_data        (ctxt_mask_dstreg_data[i]),
        .mask_rd0_data       (ctxt_mask_rd0_data[i]),
        .mask_rd1_data       (ctxt_mask_rd1_data[i]),
        .lanes_enabled       (ctxt_lanes_enabled[i])
    );
end
endgenerate

// --- REGISTER FILES --- //
wire [31:0]   ctxt_dstreg_write[MAX_WARPS];
wire          ctxt_src0_read[MAX_WARPS];
wire          ctxt_src1_read[MAX_WARPS];
// ctxt_dstreg_addr above shared with warp_contexts
// ctxt_src0_addr above shared with warp_contexts
// ctxt_src1_addr above shared with warp_contexts
wire [1023:0] ctxt_dstreg_data[MAX_WARPS];
wire [1023:0] ctxt_src0_data[MAX_WARPS];
wire [1023:0] ctxt_src1_data[MAX_WARPS];

generate
for (i = 0; i < MAX_WARPS; i = i + 1) begin : MULTIPROCESSOR_VREGFILES
    vregfile u_vregfile (
        .clk          (clk),
        .rst_n        (rst_n),
        .dstreg_write (ctxt_active[i] & ctxt_dstreg_write[i]),
        .src0_read    (ctxt_src0_read[i]),
        .src1_read    (ctxt_src1_read[i]),
        .dstreg_addr  (ctxt_dstreg_addr[i]),
        .src0_addr    (ctxt_src0_addr[i]),
        .src1_addr    (ctxt_src1_addr[i]),
        .dstreg_data  (ctxt_dstreg_data[i]),
        .src0_data    (ctxt_src0_data[i]),
        .src1_data    (ctxt_src1_data[i])
    );
end
endgenerate

// --- WARP CHANNELS --- //
wire [NUM_CHANNELS-1:0] chnl_set_active;
wire [NUM_CHANNELS-1:0] chnl_retire_channels;
wire [31:0]             chnl_mask_write[NUM_CHANNELS];
wire                    chnl_mask_read[NUM_CHANNELS];
wire [31:0]             chnl_mask_dstreg_data[NUM_CHANNELS];
wire [31:0]             chnl_mask_rd0_data[NUM_CHANNELS];
wire [31:0]             chnl_mask_rd1_data[NUM_CHANNELS];

wire [31:0]             chnl_lanes_enabled[NUM_CHANNELS];
wire [31:0]             chnl_program_counter[NUM_CHANNELS];
wire                    chnl_set_program_counter[NUM_CHANNELS];
wire [31:0]             chnl_next_program_counter[NUM_CHANNELS];
wire [31:0]             chnl_instruction[NUM_CHANNELS];
wire                    chnl_ifetch[NUM_CHANNELS];
wire                    chnl_fetched[NUM_CHANNELS];

wire [31:0]             chnl_dstreg_write[NUM_CHANNELS];
wire                    chnl_src0_read[NUM_CHANNELS];
wire                    chnl_src1_read[NUM_CHANNELS];
wire [4:0]              chnl_dstreg_addr[NUM_CHANNELS];
wire [4:0]              chnl_src0_addr[NUM_CHANNELS];
wire [4:0]              chnl_src1_addr[NUM_CHANNELS];
wire [1023:0]           chnl_dstreg_data[NUM_CHANNELS];
wire [1023:0]           chnl_src0_data[NUM_CHANNELS];
wire [1023:0]           chnl_src1_data[NUM_CHANNELS];

wire                    chnl_load[NUM_CHANNELS];
wire                    chnl_store[NUM_CHANNELS];
wire [31:0]             chnl_loadstore_address[NUM_CHANNELS];
wire [31:0]             chnl_loadstore_readdata[NUM_CHANNELS];
wire [31:0]             chnl_loadstore_writedata[NUM_CHANNELS];
wire                    chnl_loadstore_served[NUM_CHANNELS];


generate
for (i = 0; i < NUM_CHANNELS; i = i + 1) begin : MULTIPROCESSOR_WARP_CHANNELS
    warp_channel #(
        .MAX_WARPS(MAX_WARPS),
        .MASKS_PER_THREAD(MASKS_PER_THREAD)
    ) u_warp_channel (
    
        .clk              (clk),
        .rst_n            (rst_n),

        // scheduler
        .set_active       (chnl_set_active[i]),
        .new_active_warp  (),
        .active           (),
        .active_warp      (), // controls warp selection muxes
        .retire_channel   (chnl_retire_channels[i]),

        // warp info & masks
        .mask_write           (chnl_mask_write[i]),
        .mask_read            (chnl_mask_read[i]),
        .mask_dstreg_data     (chnl_mask_dstreg_data[i]),
        .mask_src0_data       (chnl_mask_rd0_data[i]),
        .mask_src1_data       (chnl_mask_rd1_data[i]),

        .lanes_enabled        (chnl_lanes_enabled[i]),

        .program_counter      (chnl_program_counter[i]),
        .set_program_counter  (chnl_set_program_counter[i]),
        .next_program_counter (chnl_next_program_counter[i]),
        .instruction          (chnl_instruction[i]),
        .ifetch               (chnl_ifetch[i]),
        .fetched              (chnl_fetched[i]),

        // register file(s)
        .dstreg_write         (chnl_dstreg_write[i]),
        .src0_read            (chnl_src0_read[i]),
        .src1_read            (chnl_src1_read[i]),
        .dstreg_addr          (chnl_dstreg_addr[i]),
        .src0_addr            (chnl_src0_addr[i]),
        .src1_addr            (chnl_src1_addr[i]),
        .dstreg_data          (chnl_dstreg_data[i]),
        .src0_data            (chnl_src0_data[i]),
        .src1_data            (chnl_src1_data[i]),

        // loadstore
        .load                 (chnl_load[i]),
        .store                (chnl_store[i]),
        .loadstore_address    (chnl_loadstore_address[i]),
        .loadstore_readdata   (chnl_loadstore_readdata[i]),
        .loadstore_writedata  (chnl_loadstore_writedata[i]),
        .loadstore_served     (chnl_loadstore_served[i])
    );
end
endgenerate

// --- WARP SCHEDULER --- //
wire [$clog2(MAX_WARPS)-1:0] channel_selector[NUM_CHANNELS];
wire [$clog2(NUM_CHANNELS)-1:0] context_selector[MAX_WARPS];
warp_scheduler #(
    .MAX_WARPS(MAX_WARPS),
    .NUM_CHANNELS(NUM_CHANNELS)
) u_warp_scheduler (
    .clk                 (clk),
    .rst_n               (rst_n),

    // command interface
    .start_kernel        (start_kernel),
    .set_threadcount     (set_threadcount),
    .new_threadcount     (new_threadcount),
    .kernel_active       (kernel_active), // block the command interface

    // switch control
    .channel_selector    (channel_selector),
    .context_selector    (context_selector),

    // warp contexts
    .set_context_enabled (ctxt_set_enabled),
    .clr_context_enabled (ctxt_clr_enabled),

    .set_context_active  (ctxt_set_active),
    .clr_context_active  (ctxt_clr_active),
    .context_active      (ctxt_active),

    // channels
    .set_channel_active  (chnl_set_active),

    // from writeback probably
    .retire_channels     (chnl_retire_channels)
);

// --- CONTEXT <--> CHANNEL SWITCH
warp_switch #(
    .MAX_WARPS(MAX_WARPS),
    .NUM_CHANNELS(NUM_CHANNELS),
    .MASKS_PER_THREAD(MASKS_PER_THREAD)
) u_warp_switch (

    // control signals from scheduler
    .channel_selector      (channel_selector),
    .context_selector      (context_selector),

    // contexts -> channels
    .ctxt_program_counter      (ctxt_program_counter),
    .chnl_program_counter      (chnl_program_counter),
    .ctxt_lanes_enabled        (ctxt_lanes_enabled),
    .chnl_lanes_enabled        (chnl_lanes_enabled),
    .ctxt_src0_data            (ctxt_src0_data),
    .chnl_src0_data            (chnl_src0_data),
    .ctxt_src1_data            (ctxt_src1_data),
    .chnl_src1_data            (chnl_src1_data),

    // channels -> contexts
    .chnl_set_program_counter  (chnl_set_program_counter),
    .ctxt_set_program_counter  (ctxt_set_program_counter),
    .chnl_next_program_counter (chnl_next_program_counter),
    .ctxt_next_program_counter (ctxt_next_program_counter),
    .chnl_dstreg_write         (chnl_dstreg_write),
    .ctxt_dstreg_write         (ctxt_dstreg_write),
    .chnl_src0_read            (chnl_src0_read),
    .ctxt_src0_read            (ctxt_src0_read),
    .chnl_src1_read            (chnl_src1_read),
    .ctxt_src1_read            (ctxt_src1_read),
    .chnl_dstreg_addr          (chnl_dstreg_addr),
    .ctxt_dstreg_addr          (ctxt_dstreg_addr),
    .chnl_src0_addr            (chnl_src0_addr),
    .ctxt_src0_addr            (ctxt_src0_addr),
    .chnl_src1_addr            (chnl_src1_addr),
    .ctxt_src1_addr            (ctxt_src1_addr),
    .chnl_dstreg_data          (chnl_dstreg_data),
    .ctxt_dstreg_data          (ctxt_dstreg_data),

    .chnl_mask_write           (chnl_mask_write),
    .ctxt_mask_write           (ctxt_mask_write),
    .chnl_mask_read            (chnl_mask_read),
    .ctxt_mask_read            (ctxt_mask_read),
    .chnl_mask_dstreg_data     (chnl_mask_dstreg_data),
    .ctxt_mask_dstreg_data     (ctxt_mask_dstreg_data)

);

// memory arbitration (ifetch only for now)
core_mem_arb #(
    .NUM_CHANNELS(NUM_CHANNELS)
) u_core_mem_arb (
    .clk                 (clk),
    .rst_n               (rst_n),
    .program_counter     (chnl_program_counter),
    .instruction         (chnl_instruction),
    .ifetch              (chnl_ifetch),
    .fetched             (chnl_fetched),
    .load                (chnl_load),
    .store               (chnl_store),
    .loadstore_address   (chnl_loadstore_address),
    .loadstore_readdata  (chnl_loadstore_readdata),
    .loadstore_writedata (chnl_loadstore_writedata),
    .loadstore_served    (chnl_loadstore_served),


    .core_address        (core_address),
    .core_burstcount     (core_burstcount),
    .core_waitrequest    (core_waitrequest),
    .core_readdata       (core_readdata),
    .core_readdatavalid  (core_readdatavalid),
    .core_read           (core_read),
    .core_writedata      (core_writedata),
    .core_byteenable     (core_byteenable),
    .core_write          (core_write)
);


endmodule
