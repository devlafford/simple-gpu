// YOU MAY NOT READ AND WRITE AT THE SAME TIME
module regfile_m10k(
    input         clk,
    input         rst_n,
    input         dstreg_write,
    input         src0_read,
    input         src1_read,
    input  [4:0]  dstreg_addr,
    input  [4:0]  src0_addr,
    input  [4:0]  src1_addr,
    input  [31:0] dstreg_data,
    output [31:0] src0_data,
    output [31:0] src1_data
);

onchip_ram (
    .clock(clk),
    .wren_a(dstreg_write),
    .wren_b(0),
    .address_a(dstreg_write ? dstreg_addr : src0_addr),
    .address_b(src1_addr),
    .data_a(dstreg_data),
    .data_b(0),
    .q_a(src0_data),
    .q_b(src1_data)
);




endmodule
