module seg7(
    input  [31:0] binary,
    output [6:0]  seg0,
    output [6:0]  seg1,
    output [6:0]  seg2,
    output [6:0]  seg3,
    output [6:0]  seg4,
    output [6:0]  seg5
);

reg [3:0]  hundredthousands;
reg [3:0]  tenthousands;
reg [3:0]  thousands;
reg [3:0]  hundreds;
reg [3:0]  tens;
reg [3:0]  ones;

integer i;
always_comb begin
    hundredthousands = 4'b0000;
    tenthousands     = 4'b0000;
    thousands        = 4'b0000;
    hundreds         = 4'b0000;
    tens             = 4'b0000;
    ones             = 4'b0000;

    for (integer i = 31; i >= 0; i = i-1) begin
        if (hundredthousands >= 5) hundredthousands = hundredthousands + 3;
        if (tenthousands     >= 5) tenthousands     = tenthousands     + 3;
        if (thousands        >= 5) thousands        = thousands        + 3;
        if (hundreds         >= 5) hundreds         = hundreds         + 3;
        if (tens             >= 5) tens             = tens             + 3;
        if (ones             >= 5) ones             = ones             + 3;

        hundredthousands     = hundredthousands << 1;
        hundredthousands[0]  = tenthousands[3];

        tenthousands     = tenthousands << 1;
        tenthousands[0]  = thousands[3];

        thousands     = thousands << 1;
        thousands[0]  = hundreds[3];

        hundreds = hundreds << 1;
        hundreds[0]  = tens[3];

        tens = tens << 1;
        tens[0]  = ones[3];

        ones = ones << 1;
        ones[0]  = binary[i];


    end

    // display task
    bcd_to_seg7(ones,             seg0);
    bcd_to_seg7(tens,             seg1);
    bcd_to_seg7(hundreds,         seg2);
    bcd_to_seg7(thousands,        seg3);
    bcd_to_seg7(tenthousands,     seg4);
    bcd_to_seg7(hundredthousands, seg5);

end

task bcd_to_seg7;
input  [3:0] digit;
output [6:0] hex;
begin
    case(digit)
        4'b0000: hex = {7'b1000000}; // 0
        4'b0001: hex = {7'b1111001}; // 1
        4'b0010: hex = {7'b0100100}; // 2
        4'b0011: hex = {7'b0110000}; // 3
        4'b0100: hex = {7'b0011001}; // 4
        4'b0101: hex = {7'b0010010}; // 5
        4'b0110: hex = {7'b0000010}; // 6
        4'b0111: hex = {7'b1111000}; // 7
        4'b1000: hex = {7'b0000000}; // 8
        4'b1001: hex = {7'b0011000}; // 9
        default: hex = {7'b1110111};
    endcase
end
endtask
endmodule
