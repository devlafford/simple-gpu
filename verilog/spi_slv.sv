


module spi_slv (
    input        spi_slave_clk_clk,
    input        spi_slave_rstn_reset_n,
    input        spi_slave_external_mosi,
    output       spi_slave_external_miso,
    input        spi_slave_external_sclk,
    input        spi_slave_external_nss,
    input        spi_slave_st_source_ready,
    output       spi_slave_st_source_valid,
    output [7:0] spi_slave_st_source_data,
    input        spi_slave_st_sink_valid, //TODO
    input  [7:0] spi_slave_st_sink_data, //TODO
    output       spi_slave_st_sink_ready //TODO
);

reg [3:0] bitcounter;
reg [7:0] shifter_in;
wire buffer_in_empty;
wire buffer_in_full;
wire clk, rst_n, spi_slave_st_external_miso;
assign clk = spi_slave_clk_clk;
assign rst_n = spi_slave_rstn_reset_n;
assign spi_slave_st_external_miso = 0;

// buffering for metastability
localparam SYNC_SIZE = 3;
localparam bit [SYNC_SIZE-1:0] SYNC_INIT = 0;
reg [SYNC_SIZE-1:0] mosi_sync;
reg               mosi;
reg [SYNC_SIZE:0] sclk_sync;
reg               sclk;
reg [SYNC_SIZE-1:0] spi_select_sync;
reg               spi_select;
wire sclk_sample;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) {mosi, mosi_sync} <= {1'b0, SYNC_INIT};
    else {mosi, mosi_sync} <= {mosi_sync, spi_slave_external_mosi};

    if (~rst_n) {sclk, sclk_sync} <= {1'b0, 1'b0, SYNC_INIT};
    else {sclk, sclk_sync} <= {sclk_sync, spi_slave_external_sclk};

    if (~rst_n) {spi_select, spi_select_sync} <= {1'b0, SYNC_INIT};
    else {spi_select, spi_select_sync} <= {spi_select_sync, spi_slave_external_nss};
end

// positive edge detector
assign sclk_sample = ~sclk & sclk_sync[SYNC_SIZE] & ~spi_select;

// input buffer
buffer8 #(
    .BUFFER_DEPTH(256)
) buffer_in (
    .clk(clk),
    .rst_n(rst_n),
    .din(shifter_in),
    .insert(bitcounter == 8),
    .remove(spi_slave_st_source_valid),
    .dout(spi_slave_st_source_data),
    .empty(buffer_in_empty),
    .full(buffer_in_full)
);
assign spi_slave_st_source_valid = spi_slave_st_source_ready & ~buffer_in_empty;
assign spi_slave_st_sink_ready = 0;


// bit counter
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) bitcounter <= 0;
    else if (sclk_sample) bitcounter <= bitcounter + 1;
    else if (bitcounter == 8) bitcounter <= 0;
    else bitcounter <= bitcounter;
end

// shift register (MSB first)
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) shifter_in <= 0;
    else if (sclk_sample) begin
        shifter_in[7:1] <= shifter_in[6:0];
        shifter_in[0] <= mosi;
    end
    else shifter_in <= shifter_in;
end

endmodule

// TODO: spi select
