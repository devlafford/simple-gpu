module warp_channel #(
    parameter MAX_WARPS = 64,
    parameter MASKS_PER_THREAD = 4
) (
    input  clk,
    input  rst_n,

    // scheduler
    input  set_active,
    input  [$clog2(MAX_WARPS)-1:0] new_active_warp,
    output reg active,
    output reg [$clog2(MAX_WARPS)-1:0] active_warp, // controls warp selection muxes
    output retire_channel,

    // warp context & masks
    output [31:0] mask_write,
    output mask_read,
    output [31:0] mask_dstreg_data,
    input  [31:0] mask_src0_data,
    input  [31:0] mask_src1_data,

    input  [31:0] lanes_enabled,

    // instruction fetch logic
    input  [31:0] program_counter,
    output        set_program_counter,
    output [31:0] next_program_counter,
    input  [31:0] instruction,
    output ifetch,
    input  fetched,
    
    // register file(s)
    output [31:0]   dstreg_write,
    output          src0_read,
    output          src1_read,
    output [4:0]    dstreg_addr,
    output [4:0]    src0_addr,
    output [4:0]    src1_addr,
    output [1023:0] dstreg_data,
    input  [1023:0] src0_data,
    input  [1023:0] src1_data,

    // loadstore
    // FIXME: these loadstore signals are not vectorized, and probably should be
    output        load,
    output        store,
    output [31:0] loadstore_address,
    input  [31:0] loadstore_readdata,
    output [31:0] loadstore_writedata,
    input         loadstore_served

);
localparam MAX_THREADS = 32*MAX_WARPS;

// FIXME: make loadstore actually work properly
// this is just a placeholder to get the synthesis tool to generate everything
genvar testgv;
generate
    for (testgv = 0; testgv < 32; testgv = testgv + 1) begin : WARP_CHANNEL_BS_LOADSTORE
        assign loadstore_address[testgv] = dstreg_data[32*testgv];
    end
endgenerate
assign next_program_counter = program_counter + 4;

// remember what warp this channel is working on
// allow active -> active transitions when changing warps
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) active <= 0;
    else if (set_active) active <= 1;
    else if (retire_channel) active <= 0;
    else active <= active;

    if (~rst_n) active_warp <= 0;
    else if (set_active) active_warp <= new_active_warp;
    else if (retire_channel) active_warp <= 0;
    else active_warp <= active_warp;
end


// TODO: pipeline and actually use these signals for something
assign retire_channel = fetched;
assign ifetch = active;

// program_counter management


// instantiate decoder
wire        unconditional;
wire [22:0] immediate;
wire        use_imm;
wire [3:0]  op;
wire [1:0]  sf;
wire        dstreg_write_single;
wire        mask_write_single;
wire [31:0] enable;
decode u_decode (
    .instruction(instruction),
    .unconditional(unconditional),
    .dstreg_write(dstreg_write_single),
    .src0_read(src0_read),
    .src1_read(src1_read),
    .dstreg_addr(dstreg_addr),
    .src0_addr(src0_addr),
    .src1_addr(src1_addr),
    .immediate(immediate),
    .use_imm(use_imm),
    .dst_mask(mask_write_single),
    .src_mask(mask_read),
    .op(op),
    .sf(sf),
    .load(load),
    .store(store),
    .global_mem(/* FUTURE */),
    .shared_mem(/* FUTURE */)
);

// final say on if a lane is enabled
genvar i;
generate
for (i = 0; i < 32; i = i + 1) begin : WARP_CHANNEL_LANE_ENABLES
    assign enable[i] = (lanes_enabled[i] | unconditional);
    assign dstreg_write[i] = enable[i] & ~mask_write_single ? dstreg_write_single : 0;
    assign mask_write[i]   = enable[i] &  mask_write_single ? mask_write_single : 0;
    assign mask_dstreg_data[i] = dstreg_data[32*i];
end
endgenerate

// instantiate valu
wire [1023:0] operand1;
generate
for (i = 0; i < 32; i = i + 1) begin : WARP_CHANNEL_OPERAND1
    assign operand1[32*i+:32] = use_imm ? immediate : src1_data[32*i+:32];
end
endgenerate
valu u_valu(
    .operand0(src0_data),
    .operand1(operand1),
    .result(dstreg_data),
    .op(op),
    .sf(sf)
);




endmodule
